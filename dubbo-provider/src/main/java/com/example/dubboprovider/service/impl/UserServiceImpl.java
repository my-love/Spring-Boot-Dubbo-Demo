package com.example.dubboprovider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.example.dubboservice.entity.User;
import com.example.dubboservice.service.UserService;
import org.springframework.stereotype.Component;

/**
 * @author Satsuki
 * @time 22点32分
 * @description: 模拟数据库事务实现
 */
//@Component
@Service
public class UserServiceImpl implements UserService {

    @Override
    public User getUser() {
        User user = new User();
        user.setId(1L);
        user.setUsername("没想到吧");
        user.setPassword("123456");
        return user;
    }
}
