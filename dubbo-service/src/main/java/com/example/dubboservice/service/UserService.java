package com.example.dubboservice.service;

import com.example.dubboservice.entity.User;

public interface UserService {
    User getUser();
}
